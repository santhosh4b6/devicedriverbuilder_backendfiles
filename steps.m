%This file has code for generating two system objects namely 1. Digital
%read 2. Digital Write using device driver API's.

% Copyright 2023 The MathWorks, Inc.

clc;
%%Steps to add custom driver
% Create a driver block object  - Digital Read
blockName = 'DigitalRead'
digitalObj = createDriverBlock(blockName,'source')
% Add Mask Description
addMaskDescription(digitalObj,'Read the logical state of input pin.')  %#ok<*NOPTS>
% Add the digital header file path
headerFile = fullfile(matlabroot,'toolbox\target\shared\svd\include\MW_digitalIO.h')
%edit(headerFile)
addHeaderFile(digitalObj,headerFile)
% Add the dependent header path
dependentHeaderPath = fullfile(matlabroot,'toolbox\target\shared\svd\include')
addHeaderPath(digitalObj,dependentHeaderPath)
% Add the digital source file path
sourceFilePath = fullfile(matlabroot,'toolbox\target\supportpackages\mbed\src\MW_digitalIO.cpp')
addSourceFile(digitalObj,sourceFilePath)
% List the functions
functionList = getFunctionsList(digitalObj)

% Add functions taht need to be called in setup,step and release
addSetupFunctions(digitalObj,functionList{1})
addStepFunctions(digitalObj,functionList{2})
addReleaseFunctions(digitalObj,functionList{4})
% Create a output property
value = createOutput(digitalObj,'value','DataType', 'uint8', 'Size', 1)
pin = createProperty(digitalObj,'Pin','DataType', 'uint32', 'Size', 1,'InitValue',1)
handle = createProperty(digitalObj,'handle','DataType', 'MW_Handle_Type', 'Size', 1,'Visible',false)
direction = createProperty(digitalObj,'direction','DataType', 'uint8', 'Size', 1,'Visible',false,'InitValue',0)
% Add arguments
%MW_Handle_Type MW_digitalIO_open(uint32_T pin, uint8_T direction)
addInputArguments(digitalObj,functionList{1},{pin,direction})
addReturnArgument(digitalObj,functionList{1},handle)

%void MW_digitalIO_write(MW_Handle_Type DigitalIOPinHandle, boolean_T value)
addInputArguments(digitalObj,functionList{2},handle)
addReturnArgument(digitalObj,functionList{2},value)

%void MW_digitalIO_write(MW_Handle_Type DigitalIOPinHandle, boolean_T value)
addInputArguments(digitalObj,functionList{4},handle)

% Generate the system object
buildCode(digitalObj)
%% Test
% tempModel = new_system()
% tempModelName = get_param(tempModel,'Name')
% open_system(tempModelName)
% add_block('simulink/User-Defined Functions/MATLAB System',[tempModelName '/MATLAB System'])
% set_param([tempModelName '/MATLAB System'],'System',blockName)


%% Digital Write
blockName = 'DigitalWrite'
digitalObj = createDriverBlock(blockName,'sink')
% Add Mask Description
addMaskDescription(digitalObj,'Set the logical state of output pin.')
% Add the digital header file path
headerFile = fullfile(matlabroot,'toolbox\target\shared\svd\include\MW_digitalIO.h')
addHeaderFile(digitalObj,headerFile)
% Add the dependent header path
dependentHeaderPath = fullfile(matlabroot,'toolbox\target\shared\svd\include')
addHeaderPath(digitalObj,dependentHeaderPath)
% Add the digital source file path
sourceFilePath = fullfile(matlabroot,'toolbox\target\supportpackages\mbed\src\MW_digitalIO.cpp')
addSourceFile(digitalObj,sourceFilePath)
% List the functions
functionList = getFunctionsList(digitalObj)
% Add functions taht need to be called in setup,step and release
addSetupFunctions(digitalObj,functionList{1})
addStepFunctions(digitalObj,functionList{3})
addReleaseFunctions(digitalObj,functionList{4})
% Create a output property
value = createInput(digitalObj,'value','DataType', 'uint8', 'Size', 1)
pin = createProperty(digitalObj,'Pin','DataType', 'uint32', 'Size', 1,'InitValue',1)
handle = createProperty(digitalObj,'handle','DataType', 'MW_Handle_Type', 'Size', 1,'Visible',false)
direction = createProperty(digitalObj,'direction','DataType', 'uint8', 'Size', 1,'Visible',false,'InitValue',1)
% Add arguments
%MW_Handle_Type MW_digitalIO_open(uint32_T pin, uint8_T direction)
addInputArguments(digitalObj,functionList{1},{pin,direction})
addReturnArgument(digitalObj,functionList{1},handle)
addInputArguments(digitalObj,functionList{3},{handle,value})
addInputArguments(digitalObj,functionList{4},handle)

% Generate the system object
buildCode(digitalObj)
%% Test
tempModel = new_system();
tempModelName = get_param(tempModel,'Name');
set_param(tempModelName,'HardwareBoard','STM32 Nucleo F767ZI');
open_system(tempModelName)
add_block('simulink/User-Defined Functions/MATLAB System',[tempModelName '/MATLAB System1'])
set_param([tempModelName '/MATLAB System1'],'System','DigitalRead')
add_block('simulink/User-Defined Functions/MATLAB System',[tempModelName '/MATLAB System2'])
set_param([tempModelName '/MATLAB System2'],'System','DigitalWrite')