% Copyright 2023 The MathWorks, Inc.
function obj = createDriverBlock(varargin)
p=inputParser;
addParameter(p,'Name','');
addParameter(p,'BlockType','');
parse(p,varargin{:})
Name = p.Results.Name;
BlockType = p.Results.BlockType;
obj = DriverBlock(Name,BlockType);
end
