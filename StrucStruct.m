classdef StrucStruct < matlab.mixin.SetGet
% Copyright 2023 The MathWorks, Inc.
    properties
        Name = '';
        Values = {};
    end
    
    methods
        function obj = StrucStruct(name,vals)
            obj.Name = name;
            for i=1:length(vals)
                a = vals{i};
                a = strsplit(a,' ');
                obj.Values = [obj.Values a{2}];
            end
        end
        
        
    end
end
