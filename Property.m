classdef Property < matlab.mixin.SetGet
    % Copyright 2023 The MathWorks, Inc.
    properties
        Name = '';
        Size = '';
        DataType = '';
        Label = '';
        InitValue;
        Tunable;
        Visible;
        Enable;
    end
    methods
        function obj = Property(name,varargin)    
            p = inputParser;
            addRequired(p,'name',@isvarname);
            addParameter(p,'Size',1);
            addParameter(p,'Label',name,@isstring);
            addParameter(p,'DataType','double',@ischar);
            addParameter(p,'Tunable',true,@islogical);
            addParameter(p,'Visible',true,@islogical);
            addParameter(p,'Enable',false,@islogical);
            addParameter(p,'InitValue','0');
            p.parse(name,varargin{:});
            classes = {'numeric'};
            attributes = {'row','nonempty'};
            validateattributes(p.Results.Size,classes,attributes);
            if(numel(p.Results.Size)>2)
                error('expected [1x1] or [1x2] vector in size parameter')
            end  
            obj.Name = convertStringsToChars(p.Results.name);
            obj.Label = convertStringsToChars(p.Results.Label);
            obj.DataType = convertStringsToChars(p.Results.DataType);
            if(numel(p.Results.Size)==1)
                obj.Size = sprintf('[%s,%s]',num2str(p.Results.Size),num2str(p.Results.Size));
            else
                obj.Size = sprintf('[%s,%s]',num2str(p.Results.Size(1)),...
                                        num2str(p.Results.Size(2)));
            end
            obj.InitValue = num2str(p.Results.InitValue);
            obj.Tunable = p.Results.Tunable;
            obj.Visible = p.Results.Visible;
            obj.Enable = p.Results.Enable;
        end
    end
end