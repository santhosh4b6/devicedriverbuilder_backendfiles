classdef ##SYSTEM_OBJECT_NAME## < matlab.System ...
        & coder.ExternalDependency ...
        & matlabshared.sensors.simulink.internal.I2CSensorBase ...
        & matlabshared.sensors.simulink.internal.BlockSampleTime
    
    % ##BLOCK_MASK_DESCR##
    % Copyright The MathWorks, Inc.
    %#codegen
    %#ok<*EMCA>
    
    properties
        ##PROPERTIES_TUNABLE##
    end
    
    properties(Access = protected)
        Logo = 'SENSORS'
    end

    properties (Nontunable)
        ##PROPERTIES_NONTUNABLE##
        I2CModule = '';
        I2CAddress;
    end
    
    properties (Access = private)
        % Pre-computed constants.
        ##PROPERTIES_PRIVATE##
    end
    
    properties(Nontunable, Access = protected)
        I2CBus;
    end

    methods
        % Constructor
        function obj = ##SYSTEM_OBJECT_NAME##(varargin)
            % Support name-value pair arguments when constructing the object.
            setProperties(obj,nargin,varargin{:});
            ##VARIABE_INIT##
        end
    end
    
    methods (Access=protected)
        function setupImpl(obj) 
            setValidatedI2CBus(obj);
            if ~coder.target('MATLAB')
                ##SETUP_CODER_CINCLUDE##
                ##SETUP_CODER_CEVAL##
            end
        end
        
        function [varargout]=stepImpl(obj ##STEP_INPUT_PARAM##)
            if isempty(coder.target)              
            else
                ##STEP_CODER_CEVAL##
            end
        end
        
        function releaseImpl(obj) 
            if isempty(coder.target)
            else
                ##RELEASE_CODER_CEVAL##
            end
        end
    end
    
    methods (Access=protected)
        %% Define input properties
        function num = getNumInputsImpl(~)
            num = ##NUM_INPUT##;
        end
        
        function num = getNumOutputsImpl(~)
            num = ##NUM_OUTPUT##;
        end
        
        function flag = isInputSizeLockedImpl(~,~)
            flag = true;
        end
        
        function varargout = isInputFixedSizeImpl(~,~)
            ##INPUT_FIXED##
        end
        
        function flag = isInputComplexityLockedImpl(~,~)
            flag = true;
        end

        function varargout   = isOutputFixedSizeImpl(obj)
            for i = 1 : obj.getNumOutputsImpl
                varargout{i} = true;
            end
        end

        function varargout  = isOutputComplexImpl(obj)
            for i = 1 : obj.getNumOutputsImpl
                varargout{i} = false;
            end
        end

        function validateInputsImpl(~ ##STEP_INPUT_PARAM##)
            if isempty(coder.target)
                % Run input validation only in Simulation
                ##VALIDATE_INPUT##
            end
        end

         % Block mask display
        function maskDisplayCmds = getMaskDisplayImpl(obj)
            outport_label = [];
            num = getNumOutputsImpl(obj);
            if num > 0
                outputs = cell(1,num);
                [outputs{1:num}] = getOutputNamesImpl(obj);
                for i = 1:num
                    outport_label = [outport_label 'port_label(''output'',' num2str(i) ',''' outputs{i} ''');' ]; %#ok<AGROW>
                end
            end
            icon = '##SYSTEM_OBJECT_NAME##';
            maskDisplayCmds = [ ...
                ['color(''white'');',...
                'plot([100,100,100,100]*1,[100,100,100,100]*1);',...
                'plot([100,100,100,100]*0,[100,100,100,100]*0);',...
                'color(''blue'');', ...
                ['text(38, 92, ','''',obj.Logo,'''',',''horizontalAlignment'', ''right'');',newline],...
                'color(''black'');'], ...
                ['text(52,50,' [''' ' icon ''',''horizontalAlignment'',''center'');' newline]]   ...
                outport_label
                ];
        end

         function sts = getSampleTimeImpl(obj)
            sts = getSampleTimeImpl@matlabshared.sensors.simulink.internal.BlockSampleTime(obj);
        end
    end
    
    methods (Static, Access=protected)
        function simMode = getSimulateUsingImpl(~)
            simMode = 'Interpreted execution';
        end
        
        function isVisible = showSimulateUsingImpl
            isVisible = false;
        end
    end
    
    methods (Static)
        function name = getDescriptiveName()
            name = '##SYSTEM_OBJECT_NAME##';
        end
        
        function b = isSupportedContext(context)
            b = context.isCodeGenTarget('rtw');
        end
        
        function updateBuildInfo(buildInfo, context)
            if context.isCodeGenTarget('rtw')
                coder.extrinsic('matlabshared.sensors.simulink.internal.getTargetHardwareName');
                targetname = coder.const(matlabshared.sensors.simulink.internal.getTargetHardwareName);
                coder.extrinsic('matlabshared.sensors.simulink.internal.getTargetSpecificFileLocationForSensors');
                fileLocation = coder.const(@matlabshared.sensors.simulink.internal.getTargetSpecificFileLocationForSensors,targetname);
                coder.extrinsic('which');
                coder.extrinsic('error');
                coder.extrinsic('message')
                funcName = [fileLocation,'.getTargetSensorUtilities'];
                functionPath = coder.const(@which,funcName);
                if ~isempty(fileLocation)
                    assert(~isempty(functionPath),message('matlab_sensors:general:FunctionNotAvailableSimulinkSensors','getTargetSensorUtilities'));
                    funcHandle = str2func(funcName);
                    hwUtilityObject = funcHandle('I2C');
                    assert(isa(hwUtilityObject,'matlabshared.sensors.simulink.internal.SensorSimulinkBase'),message('matlab_sensors:general:invalidHwObjSensorSimulink'));
                else
                    hwUtilityObject = '';
                end
                hwUtilityObject.updateBuildInfo(buildInfo, context);
                includeDir = fullfile(fileparts(mfilename('fullpath')),'include');
                addIncludePaths(buildInfo,includeDir);
                addIncludeFiles(buildInfo,'##HEADERFILE##',includeDir);
                addSourceFiles(buildInfo,'##SOURCEFILE##');
            end
        end
    end
end
