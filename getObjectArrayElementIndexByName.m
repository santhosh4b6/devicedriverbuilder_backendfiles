function index = getObjectArrayElementIndexByName(elementName,objArray)
%GETOBJECTARRAYELEMENTINDEXBYNAME Get the index of the object with a given name from the object array

% Copyright 2023 The MathWorks, Inc.

% Returns 0 if the object is not found

[~,index] = getObjectArrayElementByName(elementName,objArray);

end
