classdef DriverBlock < matlab.mixin.SetGet
    %Device Driver Class is the main class which generates the system
    %object and hook files.

    % Copyright 2023 The MathWorks, Inc.
    properties
        Name = '';
        BlockType = '';
        MaskDescription = 'System object mask';
        HeaderFile = {};
        SourceFile = {};
        HeaderPath = {};
        SourcePath = {};
        FunctionBlock = {};
        EnumBlock = {};
        StrucBlock = {};
        Inputs = {};
        Outputs = {};
        Masks = {};
        Internals = {};
        Constants = {};
        SetupFunction = {};
        StepFunction = {};
        SetupHeaderFile = {};
        ReleaseFunction = {};
    end
    properties (Hidden)
        %To do : Instead of manual list , auto add data map from rtw.h
        dataTypeMap = containers.Map({'uint8','int8','uint16','int16','uint32','int32', ...
            'uint64','int64','single','double','void*','uint8_T','int8_T','uint16_T','int16_T','uint32_T','int32_T', ...
            'uint64_T','int64_T','real32_T','real64_T','void*','boolean_T','pointer_T','void'}, ...
            {'uint8','int8','uint16','int16','uint32','int32', ...
            'uint64','int64','single','double','void*','uint8','int8','uint16','int16','uint32','int32', ...
            'uint64','int64','single','double','void*','uint8','void*','void'}) %#ok<*MCHDP>
        fnMap = containers.Map;
        enMap = containers.Map;
        strucMap = containers.Map;
        IncludeFiles = {};
        stepFnMap;
        stepInArg;
        setupFnMap;
        setupInArg;
    end

    methods (Access = 'public')

        function obj = DriverBlock(name,type)
            arguments
                name char
                type char
            end
            if ~(strcmpi(type,'source')||strcmpi(type,'sink'))
                error('Block Type can only be Sink or Source')
            end
            obj.Name = name;
            obj.BlockType = type;
        end

        function addMaskDescription(obj,descr)
            %adds Mask Description
            descr = convertStringsToChars(descr);
            obj.MaskDescription = descr;
        end

        function addHeaderFile(obj,file)
            %add header files to the object
            if(iscell(file))
                for i=1:length(file)
                    if (~isempty(obj.HeaderFile))
                        if(find(ismember(obj.HeaderFile,file{i})))
                            error('One or more files already exist');
                        end
                    end
                end
                for i=1:length(file)
                    obj.HeaderFile{end+1} = file{i};
                    getIncludeFiles(obj,file{i});
                end
            elseif(ischar(file))
                if(find(ismember(obj.HeaderFile,file)))
                    error('File already exists');
                else
                    obj.HeaderFile{end+1} = file;
                    getIncludeFiles(obj,file);
                end
            end
        end


        function deleteHeaderFile(obj,fileName)
            %delete header file 1 by 1
            idx = 0; %#ok<*NASGU>
            if(~ischar(fileName))
                error('File Name should be character array')
            end
            result=ismember(string(obj.HeaderFile),fileName);
            if((length(result==1)>0))
                idx = find(ismember(obj.HeaderFile,fileName));
                obj.HeaderFile(idx) = []; %#ok<*FNDSB>

            else
                error('File %s is not present',fileName);
            end
        end

        function addSourceFile(obj,file)
            %add Source file
            if(iscell(file))
                for i=1:length(file)
                    if(~isempty(obj.SourceFile))
                        if(find(ismember(obj.SourceFile,file{i})))
                            error('One or more files already exist');
                        end
                    end
                end
                for i=1:length(file)
                    obj.SourceFile{end+1} = file{i};
                end
            elseif(ischar(file))
                if(find(ismember(obj.SourceFile,file)))
                    error('File already exists');
                else
                    obj.SourceFile{end+1} = file;
                end
            end
        end

        function deleteSourceFile(obj,fileName)
            %deletes source file one by one
            idx = 0;
            if(~ischar(fileName))
                error('File Name should be character array')
            end
            if(ismember(obj.SourceFile,fileName))
                idx = find(ismember(obj.SourceFile,fileName));
                obj.SourceFile(idx) = [];
            else
                error('File %s is not present',fileName);
            end
        end

        function addHeaderPath(obj,path)
            %add header path
            if(ischar(path))
                if(find(ismember(obj.HeaderPath,path)))
                    error('Path Already Exists');
                else
                    obj.HeaderPath{end+1} = path;
                end
            else
                error('Path should be character array or string')
            end
        end

        function addSourcePath(obj,path)
            %add header path
            if(ischar(path))
                if(find(ismember(obj.SourcePath,path)))
                    error('Path Already Exists');
                else
                    obj.SourcePath{end+1} = path;
                end
            else
                error('Path should be character array or string')
            end
        end

        function deleteHeaderPath(obj,path)
            %deleteHeaderPath delete an existing path added to the block
            %   obj = deleteHeaderPath(obj,i) deletes the path
            %   from the HeaderPaths property of the driverBlock
            %   object obj.
            idx = 0;
            if(~ischar(path))
                error('Path should be character array')
            end
            if(find(ismember(obj.HeaderPath,path)))
                idx = find(ismember(obj.HeaderPath,path));
                obj.HeaderPath(idx) = [];
            else
                error('Path %s is not present',path);
            end
        end

        function h = createInput(obj,inputName,varargin)             %#ok<*INUSL>
            inputName = char(inputName);
            h = Input(inputName,varargin{:});
        end

        function addInput(obj,varargin)
            for i=1:nargin-1
                obj.Inputs{i}={varargin{i}};
            end
        end

        function h = createOutput(obj,outputName,varargin)
            outputName = convertStringsToChars(outputName);
            h = Output(outputName,varargin{:});
        end

        function addOutput(obj,varargin)
            for i=1:nargin-1
                obj.Outputs{i}={varargin{i}};
            end
        end

        function deleteOutput(obj,fileName)
            %delete header file 1 by 1
            idx = 0; %#ok<*NASGU>
            x= any(cellfun(@isequal, obj.Outputs{1}, repmat({fileName}, size(obj.Outputs{1}))));
            if(x)
                idx = cellfun(@isequal, obj.Outputs{1}, repmat({fileName}, size(obj.Outputs{1})));
                obj.Outputs{1}(idx) = []; %#ok<*FNDSB>
            else
                error('File %s is not present',fileName);
            end
        end

        function addSetupHeaderFile(obj,file)
            obj.SetupHeaderFile = file;
        end

        function h = createProperty(obj,propertyName,varargin)
            propertyName = convertStringsToChars(propertyName);

            h = Property(propertyName,varargin{:});
        end
        function addProperty(obj,varargin)
            for i=1:nargin-1
                obj.Masks{i}={varargin{i}};
            end
        end

        function l = getFunctionsList(obj)
            for i=1:length(obj.HeaderFile)
                parse(obj,obj.HeaderFile{i});
            end
            %add simpler method maybe for file search?
            for i = 1:length(obj.HeaderPath)
                if(obj.HeaderPath{i}(end) ~= '\')
                    for j =1:length(obj.IncludeFiles)
                        x = [obj.HeaderPath{i} '\'];
                        y = [x obj.IncludeFiles{j}];
                        if isfile(y)
                            parseInclude(obj,y);
                        end
                    end
                else
                    for j =1:length(obj.IncludeFiles)
                        x = obj.HeaderPath{i};
                        y = [x obj.IncludeFiles{j}];
                        if isfile(y)
                            parseInclude(obj,y);
                        end
                    end
                end
            end
            obj.FunctionBlock = obj.FunctionBlock';
            l = obj.FunctionBlock;
        end

        %Adds function to setup block
        function addSetupFunctions(obj,fName)
            if ~iscell(fName)
                fName = {fName};
            end
            for i=1:length(fName)
                if any(ismember(obj.SetupFunction,fName{i}))
                    error('%s Function already included',fName{i});
                end
            end

            for i=1:length(fName)
                obj.SetupFunction{end+1} = fName{i};
                s{i} = fName{i}(1:end);
                s{i} = strtrim(s{i});
                %                 s{i} = strrep(s{i},' *','*');
                if(~any(ismember(obj.FunctionBlock,s{i})))
                    obj.FunctionBlock{end+1} = s{i};
                    [a,b,c] = parseFunction(obj,s{i});
                    j = FunctionStruct(b,a,c);
                    if isempty(obj.setupFnMap)
                        obj.setupFnMap =j;
                        obj.fnMap(s{i}) = j;
                    else
                        var1 =  obj.setupFnMap.inpArgs;
                        var2 = obj.fnMap('Setup()').inpArgs;
                        obj.setupFnMap =j;
                        obj.fnMap('Setup()') = j;
                        obj.fnMap(s{i})=obj.fnMap('Setup()');
                        remove(obj.fnMap, 'Setup()');
                        obj.setupFnMap.inpArgs = var1;
                        %                         obj.fnMap(s{i}).inpArgs = var2;
                    end

                end
            end
        end

        function deleteSetupFunction(obj,fName)
            idx = 0;
            if(~ischar(fName))
                error('Function Name should be character array')
            end
            if(ismember(obj.SetupFunction,fName))
                idx = find(ismember(obj.SetupFunction,fName));
                obj.SetupFunction(idx) = [];
            else
                error('Function "%s" is not a Setup Function',fName);
            end
        end

        function [list,setupIndex,stepIndex] = getStepAndSetupIndex(obj,setupFunctionName,stepFunctionName)
            list=getFunctionsList(obj);
            setupIndex=find(ismember(list,setupFunctionName));
            stepIndex=find(ismember(list,stepFunctionName));
        end

        function addStepFunctions(obj,fName)
            if ~iscell(fName)
                fName = {fName};
            end
            for i=1:length(fName)
                if any(ismember(obj.StepFunction,fName{i}))
                    error('%s Function already included',fName{i});
                end
            end
            for i=1:length(fName)
                obj.StepFunction{end+1} = fName{i};
                s{i} = fName{i}(1:end);
                s{i} = strtrim(s{i});
                %                 s{i} = strrep(s{i},' *','*');
                if(~any(ismember(obj.FunctionBlock,s{i})))
                    obj.FunctionBlock{end+1} = s{i};
                    [a,b,c] = parseFunction(obj,s{i});
                    j = FunctionStruct(b,a,c);
                    if isempty(obj.stepFnMap)
                        obj.stepFnMap =j;
                        obj.fnMap(s{i}) = j;
                    else
                        var1 =  obj.stepFnMap.inpArgs;
                        var2 = obj.fnMap('Step()').inpArgs;
                        obj.stepFnMap =j;
                        obj.fnMap('Step()') = j;
                        obj.fnMap(s{i})=obj.fnMap('Step()')
                        remove(obj.fnMap, 'Step()');
                        obj.stepFnMap.inpArgs = var1;
                    end
                    %                     obj.fnMap(s{i}) = j;
                end
            end
        end

        function deleteStepFunction(obj,fName)
            idx = 0;
            if(~ischar(fName))
                error('Function Name should be character array')
            end
            if(ismember(obj.StepFunction,fName))
                idx = find(ismember(obj.StepFunction,fName));
                obj.StepFunction(idx) = [];
            else
                error('Function "%s" is not a Step Function',fName);
            end
        end

        function addReleaseFunctions(obj,fName)
            if ~iscell(fName)
                fName = {fName};
            end
            for i=1:length(fName)
                if ~any(ismember(obj.FunctionBlock,fName{i}))
                    error('Function not present');
                end
                if any(ismember(obj.ReleaseFunction,fName{i}))
                    error('%s Function already included',fName{i});
                end
            end
            for i=1:length(fName)
                obj.ReleaseFunction{end+1} = fName{i};
            end
        end

        function deleteReleaseFunction(obj,fName)
            idx = 0;
            if(~ischar(fName))
                error('Function Name should be character array')
            end
            if(ismember(obj.ReleaseFunction,fName))
                idx = find(ismember(obj.ReleaseFunction,fName));
                obj.ReleaseFunction(idx) = [];
            else
                error('Function "%s" is not a Release Function',fName);
            end
        end

        %Add validation to check number of passed arguments equal to that
        %of original function
        function addInputArguments(obj,fName,arguments)
            if ~iscell(arguments)
                arguments = {arguments};
            end
            if (~any(ismember(obj.StepFunction,fName))&&(~any(ismember(obj.SetupFunction,fName))))
                error('Function not present')
            end
            val = obj.fnMap(fName);
            for i=1:length(arguments)

                val.inpArgs{end+1} = arguments{i};
                if isa(arguments{i},'Property')
                    if(arguments{i}.Visible)
                        idx = getObjectArrayElementIndexByName(arguments{i}.Name,obj.Masks);
                        if isequal(idx,0)
                            obj.Masks{end+1} = arguments{i};
                        end
                    elseif(arguments{i}.Tunable)
                        idx = getObjectArrayElementIndexByName(arguments{i}.Name,obj.Internals);
                        if isequal(idx,0)
                            obj.Internals{end+1} = arguments{i};
                        end
                    else
                        idx = getObjectArrayElementIndexByName(arguments{i}.Name,obj.Constants);
                        if isequal(idx,0)
                            obj.Constants{end+1} = arguments{i};
                        end
                    end
                else
                end
            end
            obj.fnMap(fName) = val;
        end


        function addInputStepArguments(obj,cArguments,arguments)
            if ~iscell(arguments)
                arguments = {arguments};
            end
            if ~iscell(cArguments)
                cArguments = {cArguments};
            end
            for i=1:length(cArguments)
                if i == length(cArguments)
                    obj.stepInArg = [obj.stepInArg,cArguments{i}];
                else
                    obj.stepInArg = [obj.stepInArg,cArguments{i},','];
                end
            end
            if(isempty(obj.stepFnMap))
                fName = 'Step()';
                s{1} = fName(1:end);
                s{1} = strtrim(s{1});
                %                 s{1} = strrep(s{1},' *','*');
                if(~any(ismember(obj.FunctionBlock,s{1})))
                    obj.FunctionBlock{end+1} = s{1};
                    [a,b,c] = parseFunction(obj,s{1});
                    j = FunctionStruct(b,a,c);
                    obj.fnMap(s{1}) = j;
                    obj.stepFnMap =j;
                end
            else
                s{1}= obj.StepFunction{1};
            end
            val = obj.stepFnMap;
            val3 = obj.fnMap(s{1});
            for i=1:length(arguments)
                val.inpArgs{end+1} = arguments{i};
                %                 val3.inpArgs{end+1} = arguments{i};
                if isa(arguments{i},'Property')
                    if(arguments{i}.Visible)
                        idx = getObjectArrayElementIndexByName(arguments{i}.Name,obj.Masks);
                        if isequal(idx,0)
                            obj.Masks{end+1} = arguments{i};
                        end
                    elseif(arguments{i}.Tunable)
                        idx = getObjectArrayElementIndexByName(arguments{i}.Name,obj.Internals);
                        if isequal(idx,0)
                            obj.Internals{end+1} = arguments{i};
                        end
                    else
                        idx = getObjectArrayElementIndexByName(arguments{i}.Name,obj.Constants);
                        if isequal(idx,0)
                            obj.Constants{end+1} = arguments{i};
                        end
                    end
                else
                end
            end
            obj.stepFnMap = val;
            obj.fnMap(s{1}) = val;
        end

        function addInputSetupArguments(obj,cArguments,arguments)
            if ~iscell(arguments)
                arguments = {arguments};
            end
            if ~iscell(cArguments)
                cArguments = {cArguments};
            end
            %%
            for i=1:length(cArguments)
                if i == length(cArguments)
                    obj.setupInArg = [obj.setupInArg,cArguments{i}];
                else
                    obj.setupInArg = [obj.setupInArg,cArguments{i},','];
                end
            end
            if(isempty(obj.setupFnMap))
                fName = 'Setup()';
                s{1} = fName(1:end);
                s{1} = strtrim(s{1});
                %                 s{1} = strrep(s{1},' *','*');
                if(~any(ismember(obj.FunctionBlock,s{1})))
                    obj.FunctionBlock{end+1} = s{1};
                    [a,b,c] = parseFunction(obj,s{1});
                    j = FunctionStruct(b,a,c);
                    obj.fnMap(s{1}) = j;
                    obj.setupFnMap =j;
                end
            else
                s{1}= obj.SetupFunction{1};
            end
            val = obj.setupFnMap;
            val3 = obj.fnMap(s{1});
            for i=1:length(arguments)

                val.inpArgs{end+1} = arguments{i};
                %                 val3.inpArgs{end+1} = arguments{i};
                if isa(arguments{i},'Property')
                    if(arguments{i}.Visible)
                        idx = getObjectArrayElementIndexByName(arguments{i}.Name,obj.Masks);
                        if isequal(idx,0)
                            obj.Masks{end+1} = arguments{i};
                        end
                    elseif(arguments{i}.Tunable)
                        idx = getObjectArrayElementIndexByName(arguments{i}.Name,obj.Internals);
                        if isequal(idx,0)
                            obj.Internals{end+1} = arguments{i};
                        end
                    else
                        idx = getObjectArrayElementIndexByName(arguments{i}.Name,obj.Constants);
                        if isequal(idx,0)
                            obj.Constants{end+1} = arguments{i};
                        end
                    end
                else
                end
            end
            obj.setupFnMap = val;
            obj.fnMap(s{1}) = val;
        end
        % Improve function to delete all input arguments instead of one by
        % one
        function deleteInputArgument(obj,fName,argName)
            arguments
                obj
                fName char
                argName char
            end
            val = obj.fnMap(fName);
            if(getObjectArrayElementIndexByName(argName,val.inpArgs))
                index = getObjectArrayElementIndexByName(argName,val.inpArgs);
                val.inpArgs(index) = [];
                if isa(val,'Property')
                    if(getObjectArrayElementIndexByName(argName,obj.Masks))
                        idx = getObjectArrayElementIndexByName(argName,obj.Masks);
                        obj.Masks(idx) = [];
                    elseif(getObjectArrayElementIndexByName(argName,obj.Internals))
                        idx = getObjectArrayElementIndexByName(argName,obj.Internals);
                        obj.Internals(idx) = [];
                    elseif(getObjectArrayElementIndexByName(argName,obj.Constants))
                        idx = getObjectArrayElementIndexByName(argName,obj.Constants);
                        obj.Constans(idx) = [];
                    end
                else
                    if(getObjectArrayElementIndexByName(argName,obj.Inputs))
                        idx = getObjectArrayElementIndexByName(argName,obj.Inputs);
                        obj.Inputs(idx) = [];
                    end
                end
            else
                error('No input arguments named %s associated with %s',argName,fname);
            end
            obj.fnMap(fName) = val;
        end

        %Add validation to check number of passed arguments equal to that
        %of original function
        function addReturnArgument(obj,fName,arguments)
            if ~iscell(arguments)
                arguments = {arguments};
            end
            if ~any(ismember(obj.FunctionBlock,fName))
                error('Function not present')
            end
            for i=1:length(arguments)
                if ~(isa(arguments{i},'Output') || isa(arguments{i},'Property'))
                    error('Return Argument should either be of Output Class or Property Class')
                end
            end
            for i=1:length(arguments)
                x = arguments{i}.DataType;
                x = strrep(x,'*','');
                x = strtrim(x);
                if~(isKey(obj.dataTypeMap,x) || ...
                        isKey(obj.enMap,x))
                    error('Argument(s) have dataType not included');
                end
            end
            val = obj.fnMap(fName);
            %Add validation for invalid or unmatched data types
            for i=1:length(arguments)
                if(~(strcmp(val.retType,arguments{i}.DataType) || strcmp(val.retType,obj.dataTypeMap(arguments{i}.DataType))))
                    %warning('Data type %s for expected type %s',arguments{i}.DataType,val.retType)
                end
                val.retArg = arguments(i);
                if isa(arguments{i},'Property')
                    if(arguments{i}.Visible)
                        idx = getObjectArrayElementIndexByName(arguments{i}.Name,obj.Masks);
                        if isequal(idx,0)
                            obj.Masks{end+1} = arguments{i};
                        end
                    elseif(arguments{i}.Tunable)
                        idx = getObjectArrayElementIndexByName(arguments{i}.Name,obj.Internals);
                        if isequal(idx,0)
                            obj.Internals{end+1} = arguments{i};
                        end
                    else
                        idx = getObjectArrayElementIndexByName(arguments{i}.Name,obj.Constants);
                        if isequal(idx,0)
                            obj.Constants{end+1} = arguments{i};
                        end
                    end
                else
                end
            end
            obj.fnMap(fName) = val;
        end

        % Improve function to delete all return arguments for a single function instead of one by
        % one
        function deleteReturnArgument(obj,fName,argName)
            arguments
                obj
                fName char
                argName char
            end
            val = obj.fnMap(fName);
            if(getObjectArrayElementIndexByName(argName,val.retArg))
                index = getObjectArrayElementIndexByName(argName,val.retArg);
                val.retArg(index) = [];
                if isa(val,'Property')
                    if(getObjectArrayElementIndexByName(argName,obj.Masks))
                        idx = getObjectArrayElementIndexByName(argName,obj.Masks);
                        obj.Masks(idx) = [];
                    elseif(getObjectArrayElementIndexByName(argName,obj.Internals))
                        idx = getObjectArrayElementIndexByName(argName,obj.Internals);
                        obj.Internals(idx) = [];
                    elseif(getObjectArrayElementIndexByName(argName,obj.Constants))
                        idx = getObjectArrayElementIndexByName(argName,obj.Constants);
                        obj.Constans(idx) = [];
                    end
                else
                    if(getObjectArrayElementIndexByName(argName,obj.Outputs))
                        idx = getObjectArrayElementIndexByName(argName,obj.Outputs);
                        obj.Outputs(idx) = [];
                    end
                end
            else
                error('No return arguments named %s associated with %s',argName,fname);
            end
            obj.fnMap(fName) = val;
        end

        function generateSensorBlockAndTemplateHooks(obj,path)
            %builds the necessary files
            generateSystemObjectFile(obj,path);
        end




    end
    methods(Access = 'private')
        function getIncludeFiles(obj,file)
            % collects extra header files from main test header file
            data = fileread(file);
            incPattern = '^(#include)\s*\"[\w|.]*?\"';
            [~,~,~,s,~,~] = regexp(data,incPattern,'lineanchors');
            for i=1:numel(s)
                s{i} = s{i}(1:end-1);
                s{i} = strtrim(strsplit(s{i},'"'));
                if(~any(ismember(obj.IncludeFiles,s{i}{2})))
                    obj.IncludeFiles{end+1} = s{i}{2};
                end
            end
        end

        function parseInclude(obj,fileName)
            % parses the dependent header files for defines and enums
            data = fileread(fileName);
            data = strrep(data,' *','*');
            %parse defines
            defPattern = '^(typedef)[\w|*|\s]*?\;';
            [~,~,~,s,~,~] = regexp(data,defPattern,'lineanchors');
            for i=1:numel(s)
                s{i} = s{i}(8:end-1);
                s{i} = strtrim(s{i});
                s{i} = strrep(s{i}, ' *', '*');
                temp = strtrim(split(s{i},' '));
                if(~isKey(obj.dataTypeMap,temp{2}))
                    if isKey(obj.dataTypeMap,temp{1})
                        obj.dataTypeMap(temp{2}) = obj.dataTypeMap(temp{1});
                    else
                        obj.dataTypeMap(temp{2}) = temp{1};
                    end
                end
            end
            %parse enums
            enumPattern = '(enum)[^\;]*?\;';
            [~,~,~,s,~,~] = regexp(data,enumPattern,'lineanchors');
            for i=1:numel(s)
                s{i} = s{i}(5:end-1);
                s{i} = strtrim(s{i});
                x = strsplit(s{i},'{');
                m = strsplit(s{i},'}');
                if(~isempty(strtrim(x{1})))
                    eName = strtrim(x{1});
                else
                    eName = strtrim(m{2});
                end
                if(~any(ismember(obj.EnumBlock,eName)))

                    a = s{i};
                    eVals = extractBetween(a,'{','}');
                    prev = 0;
                    str = '';
                    for k=1:numel(eVals)

                        if contains(eVals{k},'=')
                            eVals{k} = strrep(eVals{k},'=','(');
                            eVals{k} = [eVals{k} ')'];
                            val = extractBetween(eVals{k},'(',')');
                            val = strtrim(val{1});
                            prev = str2num(val) + 1; %#ok<*ST2NM>
                        else
                            val = sprintf('(%d)',prev);
                            eVals{k} = [eVals{k} val];
                            prev = prev + 1 ;
                        end
                        str = sprintf([str '%s\n'],eVals{k});
                    end
                    j = EnumStruct(eName,str);
                    obj.enMap(eName) = j;
                    obj.EnumBlock{end+1} = eName;
                end
            end
        end

        function parse(obj,fileName)
            %parses the main header file
            data = fileread(fileName);
            data = strrep(data,' *','*');
            %parse defines
            defPattern = '^(typedef)[\w|*|\s]*?\;';
            [~,~,~,s,~,~] = regexp(data,defPattern,'lineanchors');
            for i=1:numel(s)
                s{i} = s{i}(8:end-1);
                s{i} = strtrim(s{i});
                s{i} = strrep(s{i}, ' *', '*');
                temp = strtrim(split(s{i},' '));
                if(~isKey(obj.dataTypeMap,temp{2}))
                    if isKey(obj.dataTypeMap,temp{1})
                        obj.dataTypeMap(temp{2}) = obj.dataTypeMap(temp{1});
                    else
                        obj.dataTypeMap(temp{2}) = temp{1};
                    end
                end
            end
            % parse functions
            functionPattern = '^[\w|*|\s]*\w*?\([\w|\s|\,\*]*?\)\;';
            [~,~,~,s,~,~] = regexp(data,functionPattern,'lineanchors');
            for i=1:numel(s)
                s{i} = s{i}(1:end-1);
                s{i} = strtrim(s{i});
                s{i} = strrep(s{i},' *','*');
                if(~any(ismember(obj.FunctionBlock,s{i})))
                    obj.FunctionBlock{end+1} = s{i};
                    [a,b,c] = parseFunction(obj,s{i});
                    j = FunctionStruct(b,a,c);
                    obj.fnMap(s{i}) = j;
                end
            end
        end

        function [output,functionName,argumentType] = parseFunction(~,functionString)
            %parses individual elements from a function string
            if ~isempty(functionString)
                [~,~,~,d,~,~] = regexp(functionString,'^[\w|\s|*]*?\(','lineanchors');
                x = strsplit(d{1}(1:end-1),' ');
                output = x{1};
                if(length(x)>1)
                    functionName = x{2};
                else
                    functionName = x{1};
                end
                x = strsplit(functionString((length(d{1})+1):end-1),',');

                argumentType = {};
                for i=1:length(x)
                    x{i} = strtrim(x{i});
                    temp = strsplit(x{i},' ');
                    if numel(temp)>0 && ~isempty(temp{end})
                        argumentType{end+1} = temp{1}; %#ok<*AGROW>
                    end
                end
            else
                output = '';
                functionName = '';
                argumentType = {''};
            end
        end

        function generateSystemObjectFile(obj,path)
            %generate the main matlab system object file
            % To do add print functions for non tunable mask parameters
            blockName = obj.Name;
            fileName = [blockName '.m'];
            obj.SetupHeaderFile = [blockName '.h'];
            [~,headerFileName] = fileparts(obj.SetupHeaderFile);
            propTunable = '';
            propNonTunable ='';
            propertyInitNonTunable = '';
            for i = 1:numel(obj.Masks)
                if(obj.Masks{i}{1}.Tunable)
                    propTunable = sprintf([propTunable '%s = %s(%s);\n'],obj.Masks{i}{1}.Name,obj.Masks{i}{1}.DataType,obj.Masks{i}{1}.InitValue);
                else
                    propNonTunable = sprintf([propNonTunable '%s = %s(%s);\n'],obj.Masks{i}{1}.Name,obj.Masks{i}{1}.DataType,obj.Masks{i}{1}.InitValue);
                end
                % end
                propertyInitNonTunable = sprintf([propertyInitNonTunable 'obj.%s = %s(%s);\n'],obj.Masks{i}{1}.Name,obj.Masks{i}{1}.DataType,obj.Masks{i}{1}.InitValue);

            end
            propTunable=strtrim(propTunable);
            propNonTunable = strtrim(propNonTunable);
            propertyInitNonTunable=strtrim(propertyInitNonTunable);

            propInternal = '';
            propertyInitInternal = '';
            for i = 1:numel(obj.Internals)
                if isKey(obj.enMap,obj.Internals{i}.DataType)
                    sz = extractBetween(obj.Internals{i}.Size,'[',']');
                    sz = sz{1};
                    propInternal = sprintf([propInternal '%s (%s) %s = %s.%s;\n'],obj.Internals{i}.Name,sz,obj.Internals{i}.DataType,obj.Internals{i}.DataType,obj.Internals{i}.InitValue);
                else
                    propInternal = sprintf([propInternal '%s;\n'],obj.Internals{i}.Name);
                end
                value = obj.Internals{i};
                if contains(value.DataType ,'*')||contains(obj.dataTypeMap(value.DataType) ,'*')
                    propertyInitInternal = sprintf([propertyInitInternal 'obj.%s = coder.opaque(''%s'');\n'],value.Name,value.DataType);
                else
                    propertyInitInternal = sprintf([propertyInitInternal 'obj.%s = %s(%s);\n'],value.Name,value.DataType,value.InitValue);
                end
            end
            propInternal=strtrim(propInternal);
            propertyInitInternal=strtrim(propertyInitInternal);
            propConst = '';
            propertyInitConstant = '';
            for i = 1:numel(obj.Constants)
                if isKey(obj.enMap,obj.Constants{i}.DataType)
                    sz = extractBetween(obj.Constants{i}.Size,'[',']');
                    sz = sz{1};
                    propConst = sprintf([propConst '%s (%s) %s = %s.%s;\n'],obj.Constants{i}.Name,sz,obj.Constants{i}.DataType,obj.Constants{i}.DataType,obj.Constants{i}.InitValue);
                else
                    propConst = sprintf([propConst '%s;\n'],obj.Constants{i}.Name);
                end
                value = obj.Constants{i};
                if contains(value.DataType ,'*') || contains(obj.dataTypeMap(value.DataType) ,'*')
                    propertyInitConstant = sprintf([propertyInitConstant 'obj.%s = coder.opaque(''%s'');\n'],value.Name,value.DataType);
                else
                    propertyInitConstant = sprintf([propertyInitConstant 'obj.%s = %s(%s);\n'],value.Name,value.DataType,value.InitValue);
                end
            end
            propConst=strtrim(propConst);
            propertyInitConstant=strtrim(propertyInitConstant);
            if isempty(obj.SetupFunction)
                setupName = ['void setupFunction(',obj.setupInArg,')'];
                addSetupFunctions(obj,{setupName});
            end
            if isempty(obj.StepFunction)
                stepName = ['void stepFunction(',obj.stepInArg,')'];
                addStepFunctions(obj,{stepName});
            end
            setupCoderCEval = '';
            for i = 1:numel(obj.SetupFunction)
                setupCoderCEval = sprintf([setupCoderCEval '%s\n'],getFunctionCall(obj,obj.SetupFunction{i}));
                setupFunctionName = obj.SetupFunction{i};
            end
            setupCoderCEval=strtrim(setupCoderCEval);
            stepCoderCEval = '';
            for i = 1:numel(obj.StepFunction)
                stepCoderCEval = sprintf([stepCoderCEval '%s\n'],getFunctionCall(obj,obj.StepFunction{i}));
                stepFunctionName = obj.StepFunction{i};
            end
            stepCoderCEval=strtrim(stepCoderCEval);
            releaseCoderCEval = '';
            for i = 1:numel(obj.ReleaseFunction)
                releaseCoderCEval = sprintf([releaseCoderCEval '%s\n'],getFunctionCall(obj,obj.ReleaseFunction{i}));
            end
            releaseCoderCEval=strtrim(releaseCoderCEval);
            if strcmpi(obj.BlockType,'source')
                stepInput=''
                if numel(obj.Inputs)>0
                    for i = 1:length(obj.Inputs)
                        stepInput=[stepInput,',',obj.Inputs{i}{1}.Name];
                    end
                end
                if numel(obj.Outputs)>0
                    if numel(obj.Outputs)<2
                        propOut = obj.Outputs{1}{1}.Name;
                        stepOutInit = sprintf('%s = %s(zeros(%s,%s));\n',obj.Outputs{1}{1}.Name,obj.Outputs{1}{1}.DataType,char(extractBefore(extractBetween(obj.Outputs{1}{1}.Size,'[',']'),',')),char(extractAfter(extractBetween(obj.Outputs{1}{1}.Size,'[',']'),',')));
                    else
                        propOut = ['[' obj.Outputs{1}{1}.Name];
                        stepOutInit = sprintf('%s = %s(zeros(1,%s));\n',obj.Outputs{1}{1}.Name,obj.Outputs{1}{1}.DataType,char(extractAfter(extractBetween(obj.Outputs{1}{1}.Size,'[',']'),',')));
                        for i = 2:length(obj.Outputs)
                            propOut = sprintf([propOut ',%s'],obj.Outputs{i}{1}.Name);
                            %                             stepOutInit = sprintf([stepOutInit '%s = %s(%s);\n'],obj.Outputs{1}{1}.Name,obj.Outputs{1}{1}.DataType,obj.Outputs{1}{1}.InitValue);
                            stepOutInit = sprintf([stepOutInit '%s = %s(zeros(%s,%s));\n'],obj.Outputs{i}{1}.Name,obj.Outputs{i}{1}.DataType,char(extractBefore(extractBetween(obj.Outputs{i}{1}.Size,'[',']'),',')),char(extractAfter(extractBetween(obj.Outputs{i}{1}.Size,'[',']'),',')));
                        end
                        propOut =[propOut ']'];
                        propOut = strtrim(propOut);
                        stepOutInit = strtrim(stepOutInit);
                    end
                    propOut = [propOut ' = '];
                else
                    propOut = '';
                    stepOutInit = '';
                end
                fixedOutput = '';
                complexOutput = '';
                sizeOutput = '';
                validateOutput = '';
                outputNames = '';
                for i = 1:numel(obj.Outputs)
                    fixedOutput = sprintf([fixedOutput 'varargout{%d} = true;\n'],uint8(i));
                    outputNames = sprintf([outputNames 'varargout{%d} = ''%s'';\n'],uint8(i),obj.Outputs{i}{1}.Name);
                    complexOutput = sprintf([complexOutput 'varargout{%d} = false;\n'],uint8(i));
                    sizeOutput = sprintf([sizeOutput 'varargout{%d} = %s;\n'],uint8(i),obj.Outputs{i}{1}.Size);
                    validateOutput = sprintf([validateOutput 'varargout{%d} = ''%s'';\n'],uint8(i),obj.Outputs{i}{1}.DataType);
                end
                fixedOutput=strtrim(fixedOutput);
                outputNames=strtrim(outputNames);
                complexOutput=strtrim(complexOutput);
                sizeOutput=strtrim(sizeOutput);
                validateOutput=strtrim(validateOutput);
                sourceFile ='';
                for i = 1:numel(obj.SourceFile)
                    [filepath,name,ext] = fileparts(obj.SourceFile{i});
                    sourceFileLocation = sprintf(['''%s'''],char(filepath));
                    sourceFile = [sourceFile 'addSourceFiles(buildInfo,'''  char([name,ext])   ''',' sourceFileLocation ');'];
                    %                     sourceFile = sprintf([sourceFile 'addSourceFiles(buildInfo,' '''%s'''  ',' sourceFileLocation ');\n'],char(obj.SourceFile{i}));
                end
                sourceFile = [sourceFile 'addSourceFiles(buildInfo,''' [blockName '.cpp'] ''',''' path ''');'];
                obj.SourceFile{end+1} = [blockName '.cpp'];
                headerPaths ='';
                for i= 1:numel(obj.HeaderPath)
                    headerPaths = [headerPaths sprintf(['buildInfo.addIncludePaths(', '''%s''',');\n'],obj.HeaderPath{i})];
                end
                processTemplateFile(obj,'Source.m',fullfile(path,fileName),...
                    {'##SYSTEM_OBJECT_NAME##',blockName,...
                    '##BLOCK_MASK_DESCR##',obj.MaskDescription,...
                    '##PROPERTIES_TUNABLE##',propTunable,...
                    '##PROPERTIES_NONTUNABLE##',propNonTunable,...
                    '##PROPERTIES_PRIVATE##',sprintf('%s\n%s',propConst,propInternal),...
                    '##SETUP_CODER_CINCLUDE##',['coder.cinclude(''' headerFileName '.h'');'],...
                    '##SETUP_CODER_CEVAL##',setupCoderCEval,...
                    '##STEP_RETURN_PARAM##',propOut,...
                    '##STEP_INPUT_PARAM##',stepInput,...
                    '##STEP_OUPUT_INIT##',stepOutInit,...
                    '##STEP_CODER_CEVAL##',stepCoderCEval,...
                    '##RELEASE_CODER_CEVAL##',releaseCoderCEval,...
                    '##NUM_INPUT##',num2str(numel(obj.Inputs)),...
                    '##NUM_OUTPUT##',num2str(numel(obj.Outputs)),...
                    '##OUTPUT_FIXED##',fixedOutput,...
                    '##OUTPUT_NAME##',outputNames,...
                    '##OUTPUT_COMPLEX##',complexOutput,...
                    '##OUTPUT_SIZE##',sizeOutput,...propTunabl
                    '##OUTPUT_DATATYPE##',validateOutput,...
                    '##HEARDERPATHS##',headerPaths,...
                    '##SOURCEFILE##',sourceFile...
                    })
                fileName1 = fullfile(path,[blockName '.h']);
                fileName2 = fullfile(path,[blockName '.cpp']);
                includeFileName = ['"' fileName1 '"'];
                processTemplateFile(obj,'Hook.h',fileName1,{'##SETUP_FUNCTION_NAME##',[setupFunctionName ';'], '##STEP_FUNCTION_NAME##',[stepFunctionName ';']});
                processTemplateFile(obj,'Hook.cpp',fileName2,{'##SETUP_FUNCTION_NAME##',setupFunctionName, '##STEP_FUNCTION_NAME##',stepFunctionName,'##HEADER_FILE##',includeFileName});
            else
                propIn = '';
                for i = 1:numel(obj.Inputs)
                    propIn = sprintf([propIn ',%s'],obj.Inputs{i}.Name);
                end
                propIn=strtrim(propIn);
                fixedInput = '';
                validateInput = '';
                for i = 1:numel(obj.Inputs)
                    fixedInput = sprintf([fixedInput 'varargout{%d} = true;\n'],uint8(i));
                    validateInput = sprintf([validateInput 'validateattributes(%s,{''%s''},{''scalar''},'''',''%s'');\n'],obj.Inputs{i}.Name,obj.Inputs{i}.DataType,obj.Inputs{i}.Name);
                end
                fixedInput=strtrim(fixedInput);
                validateInput=strtrim(validateInput);
                processTemplateFile(obj,'Sink.m',fileName,...
                    {'##SYSTEM_OBJECT_NAME##',blockName,...
                    '##BLOCK_MASK_DESCR##',obj.MaskDescription,...
                    '##PROPERTIES_TUNABLE##',propTunable,...
                    '##PROPERTIES_NONTUNABLE##','',...
                    '##PROPERTIES_PRIVATE##',sprintf('%s\n%s',propConst,propInternal),...
                    '##VARIABE_INIT##',sprintf('%s\n%s\n%s',propertyInitNonTunable,propertyInitConstant,propertyInitInternal),...
                    '##SETUP_CODER_CINCLUDE##',['coder.cinclude(''' headerFileName '.h'');'],...
                    '##SETUP_CODER_CEVAL##',setupCoderCEval,...
                    '##STEP_INPUT_PARAM##',propIn,...
                    '##STEP_CODER_CEVAL##',stepCoderCEval,...
                    '##RELEASE_CODER_CEVAL##',releaseCoderCEval,...
                    '##NUM_INPUT##',num2str(numel(obj.Inputs)),...
                    '##NUM_OUTPUT##',num2str(numel(obj.Outputs)),...
                    '##INPUT_FIXED##',fixedInput,...
                    '##VALIDATE_INPUT##',validateInput,...
                    '##HEADERFILE##',[headerFileName '.h'],...
                    '##SOURCEFILE##',sourceFile...
                    })

            end
            h = matlab.desktop.editor.openDocument(fullfile(path,fileName));
            h.smartIndentContents;
            h.save;
            h.close;
        end

        function processTemplateFile(~,inputFile,outputFile,args)
            %PROCESSTEMPLATEFILE
            %
            % processTemplateFile(inputFile,outputFile,varargin)
            %
            % Example:
            %
            % processTemplateFile('boardClassTemplate.m','chippro.m',...
            %    {'##BOARDNAME##','chippro','boardClassTemplate','chippro'})

            % read whole file data into an array
            if nargin > 0
                inputFile = convertStringsToChars(inputFile);
            end
            if nargin > 1
                outputFile = convertStringsToChars(outputFile);
            end
            validateattributes(args,{'cell'},{'nonempty'},'processTemplateFile','args');
            fid = fopen(inputFile,'r');
            txt = fread(fid,'*char')';
            fclose(fid);

            % Replace the Search string with Replace String
            for i = 1:2:numel(args)
                txt = strrep(txt,args{i},args{i+1});
            end
            % Write it back into the file
            fid  = fopen(outputFile,'w');
            fwrite(fid,txt,'char');
            fclose(fid);
        end

        function generateEnumFiles(obj)
            %generates enum class files for parsed enums
            for i=1:numel(obj.EnumBlock)
                outputFile = [obj.EnumBlock{i} '.m'];
                enumInit = '';
                info = obj.enMap(obj.EnumBlock{i});
                enumInit = sprintf('classdef %s < Simulink.IntEnumType\nenumeration\n%send\nend\n\n',info.Name,info.Values{1});
                fid  = fopen(outputFile,'w');
                fwrite(fid,enumInit,'char');
                fclose(fid);
                h = matlab.desktop.editor.openDocument(fullfile(pwd,outputFile));
                h.smartIndentContents;
                h.save;
                h.close;
            end
        end

        function results = getFunctionCall(obj,fun)
            %returns proper string for functions to include in system
            %object file
            info = obj.fnMap(fun);
            ret = info.retType;
            returnString = '';
            if ~isempty(info.retArg)
                if ~isequal(ret,'void')
                    value = info.retArg{1};
                    if ~isa(value,'Output')
                        returnString = sprintf('obj.%s = ',value.Name);
                    else
                        returnString = sprintf('%s = ',value.Name);
                    end
                end
            end
            arg = info.inpArgs;
            argString = '';
            for i=1:numel(arg)
                value = arg{i};
                if isa(value,'Output')
                    szy = char(extractAfter(extractBetween(value.Size,'[',']'),','));
                    szx = char(extractBefore(extractBetween(value.Size,'[',']'),','));
                    if str2num(szx)>1
                        argString = sprintf([argString ',coder.ref(%s),%s,%s'],value.Name,szx,szy);
                    else
                        argString = sprintf([argString ',coder.ref(%s),%s'],value.Name,szy);
                    end
                elseif ~isa(value,'Input')
                    szy = char(extractAfter(extractBetween(value.Size,'[',']'),','));
                    szx = char(extractBefore(extractBetween(value.Size,'[',']'),','));
                    if str2num(szx)>1
                        argString = sprintf([argString ', coder.ref(obj.%s),%s,%s'],value.Name,szx,szy);
                    else
                        argString = sprintf([argString ', coder.ref(obj.%s),%s'],value.Name,szy);
                    end
                else
                    szy = char(extractAfter(extractBetween(value.Size,'[',']'),','));
                    szx = char(extractBefore(extractBetween(value.Size,'[',']'),','));
                    if str2num(szx)>1
                        argString = sprintf([argString ', %s,%s,%s'],value.Name,szx,szy);
                    else
                        argString = sprintf([argString ', %s,%s'],value.Name,szy);
                    end
                end
            end
            results = sprintf('%scoder.ceval(''%s''%s);',returnString,info.Name,argString);
        end
    end
end