% Copyright 2023 The MathWorks, Inc.

blockName = ##Block_Name##;
blockType = ##Block_Type##;
drvObj = createDriverBlock('Name',blockName,'BlockType',blockType);

maskDescription = ##Block_Mask_Description##;
addMaskDescription(drvObj,maskDescription);

##Headerfile_Path##
##Sourcefile_Path##
##Sourcefiles##
##Output_Declaration##
##Addoutput##
##Input_Declaration##
##Addinput##
##Property_Declaration##
##Addproperty##

##CinputArguments##
##MinputArguments##

##CinputSetupArguments##
##MinputSetupArguments##

##addStepArguments##
##addSetupArguments##

##WorkingDirectory##