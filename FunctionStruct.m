classdef FunctionStruct < matlab.mixin.SetGet
% Copyright 2023 The MathWorks, Inc.
    
    properties
        Name = '';
        retType = '';
        argTypes = {};
        inpArgs = {};
        retArg = {};
    end
    
    methods
        function obj = FunctionStruct(name,ret,args)
            obj.Name = name;
            obj.retType = ret;
            obj.argTypes = args;
        end   
    end
end

