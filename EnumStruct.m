classdef EnumStruct < matlab.mixin.SetGet
    % Copyright 2023 The MathWorks, Inc.
    properties
        Name = '';
        Values = {};
    end
    
    methods
        function obj = EnumStruct(name,vals)
            obj.Name = name;
            obj.Values = [obj.Values vals];
        end
        
        
    end
end

