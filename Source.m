classdef ##SYSTEM_OBJECT_NAME## < matlab.System ...
       & coder.ExternalDependency ...
       & matlabshared.sensors.simulink.internal.BlockSampleTime
    
    % ##BLOCK_MASK_DESCR##
    % Copyright The MathWorks, Inc.
    %#codegen
    %#ok<*EMCA>
    
    properties
        ##PROPERTIES_TUNABLE##
    end

    properties(Access = protected)
        Logo = 'App generated'
    end
    
    properties (Nontunable)
        ##PROPERTIES_NONTUNABLE##      
    end
    
    properties (Access = private)
        ##PROPERTIES_PRIVATE##
    end

    methods
        % Constructor
        function obj = ##SYSTEM_OBJECT_NAME##(varargin)
            setProperties(obj,nargin,varargin{:});
        end
    end
    
    methods (Access=protected)
        function setupImpl(obj)
            if ~coder.target('MATLAB')
                ##SETUP_CODER_CINCLUDE##
                ##SETUP_CODER_CEVAL##
            end
        end
        
        function ##STEP_RETURN_PARAM##stepImpl(obj ##STEP_INPUT_PARAM##) 
            ##STEP_OUPUT_INIT##
            if isempty(coder.target)        
            else
                ##STEP_CODER_CEVAL##
            end
        end
        
        function releaseImpl(obj)
            if isempty(coder.target)
            else
                ##RELEASE_CODER_CEVAL##
            end
        end
    end
    
    methods (Access=protected)
        %% Define output properties
        function num = getNumInputsImpl(~)
            num = ##NUM_INPUT##;
        end
        
        function num = getNumOutputsImpl(~)
            num = ##NUM_OUTPUT##;
        end
        
        function varargout = getOutputNamesImpl(obj)
             ##OUTPUT_NAME##
        end

        function flag = isOutputSizeLockedImpl(~,~)
            flag = true;
        end
        
        function varargout = isOutputFixedSizeImpl(~,~)
            ##OUTPUT_FIXED##
        end
        
        function varargout = isOutputComplexImpl(~)
            ##OUTPUT_COMPLEX##
        end
        
        function varargout = getOutputSizeImpl(~)
            ##OUTPUT_SIZE##
        end
        
        function varargout = getOutputDataTypeImpl(~)
            ##OUTPUT_DATATYPE##
        end

        function maskDisplayCmds = getMaskDisplayImpl(obj)
            outport_label = [];
            num = getNumOutputsImpl(obj);
            if num > 0
                outputs = cell(1,num);
                [outputs{1:num}] = getOutputNamesImpl(obj);
                for i = 1:num
                    outport_label = [outport_label 'port_label(''output'',' num2str(i) ',''' outputs{i} ''');' ]; %#ok<AGROW>
                end
            end
            icon = '##SYSTEM_OBJECT_NAME##';
            maskDisplayCmds = [ ...
                ['color(''white'');',...
                'plot([100,100,100,100]*1,[100,100,100,100]*1);',...
                'plot([100,100,100,100]*0,[100,100,100,100]*0);',...
                'color(''blue'');', ...
                ['text(38, 92, ','''',obj.Logo,'''',',''horizontalAlignment'', ''right'');',newline],...
                'color(''black'');'], ...
                ['text(52,50,' [''' ' icon ''',''horizontalAlignment'',''center'');' newline]]   ...
                outport_label
                ];
        end

        function sts = getSampleTimeImpl(obj)
            sts = getSampleTimeImpl@matlabshared.sensors.simulink.internal.BlockSampleTime(obj);
        end
    end
    
    methods (Static, Access=protected)
        function simMode = getSimulateUsingImpl(~)
            simMode = 'Interpreted execution';
        end
        
        function isVisible = showSimulateUsingImpl
            isVisible = false;
        end
    end
    
    methods (Static)
        function name = getDescriptiveName()
            name = '##SYSTEM_OBJECT_NAME##';
        end
        
        function b = isSupportedContext(context)
            b = context.isCodeGenTarget('rtw');
        end
        
        function updateBuildInfo(buildInfo, context)
                coder.extrinsic('matlabshared.sensors.simulink.internal.getTargetHardwareName');
                targetname = coder.const(matlabshared.sensors.simulink.internal.getTargetHardwareName);
                % Get the filelocation of the SPKG specific files
                coder.extrinsic('matlabshared.sensors.simulink.internal.getTargetSpecificFileLocationForSensors');
                fileLocation = coder.const(@matlabshared.sensors.simulink.internal.getTargetSpecificFileLocationForSensors,targetname);
                coder.extrinsic('which');
                coder.extrinsic('error');
                coder.extrinsic('message');
                funcName = [fileLocation,'.getTargetSensorUtilities'];
                functionPath = coder.const(@which,funcName);
                % Only if the the path exist
                if ~isempty(fileLocation)
                    % internal error to see if the target author has provided
                    % the expected function in the specified file location
                    assert(~isempty(functionPath),message('matlab_sensors:general:FunctionNotAvailableSimulinkSensors','getTargetSensorUtilities'));
                    funcHandle = str2func(funcName);
                    hwUtilityObject = funcHandle('I2C');
                    assert(isa(hwUtilityObject,'matlabshared.sensors.simulink.internal.SensorSimulinkBase'),message('matlab_sensors:general:invalidHwObjSensorSimulink'));
                else
                    hwUtilityObject = '';
                end
                hwUtilityObject.updateBuildInfo(buildInfo, context);
                ##HEARDERPATHS##
                ##SOURCEFILE##
           
        end
    end
end
